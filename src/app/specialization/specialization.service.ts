import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Specialization } from './specialization';

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {

  constructor(private http:HttpClient) { }

  baseUrl:string = "http://localhost:9090/spec/";

  saveSpecialization(spec:Specialization):Observable<any>{
    return this.http.post(`${this.baseUrl}save`, spec, {responseType:'text'});
  }

  getAllSpecialization():Observable<Specialization[]>{
    return this.http.get<Specialization[]>(`${this.baseUrl}all`);
  }

  deleteSpecialization(id:number):Observable<any>{

    return this.http.delete(`${this.baseUrl}delete/${id}`, {responseType:'text'});
  }

  getOneSpecialization(id:number):Observable<Specialization>{
    return this.http.get<Specialization>(`${this.baseUrl}edit/${id}`);
  }

  updateSpecialization(spec:Specialization):Observable<any>{

    return this.http.put(`${this.baseUrl}update`, spec, {responseType:'text'});
  }


}
