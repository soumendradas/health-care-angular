import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { SpecDataComponent } from './spec-data/spec-data.component';
import { SpecEditComponent } from './spec-edit/spec-edit.component';
import { SpecRegisterComponent } from './spec-register/spec-register.component';


const routes: Routes =[
  {path:"spec",
  children:[
    {path: 'register', component: SpecRegisterComponent},
    {path:"all", component:SpecDataComponent},
    {path:"edit/:id", component:SpecEditComponent},
  ]
}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule]
})
export class SpecializationRoutingModule { }
