import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Specialization } from '../specialization';
import { SpecializationService } from '../specialization.service';

@Component({
  selector: 'app-spec-data',
  templateUrl: './spec-data.component.html',
  styleUrls: ['./spec-data.component.css']
})
export class SpecDataComponent implements OnInit {

  constructor(private service: SpecializationService,
              private route: Router) { }

  specs:Specialization[] = [];
  message:string = ""
  

  ngOnInit(): void {
    this.getSpecData();
  }

  getSpecData(){
    this.service.getAllSpecialization().subscribe(
      data=>{
        this.specs = data;
      },error=>{
        this.message = error;
      }

    )
  }

  trackById(index:number, spec:Specialization){
    return spec.specId;
  }

  deleteSpec(id:number){
    this.service.deleteSpecialization(id).subscribe(
      data=>{
        this.message = data;
        this.getSpecData();
      }
    )
  }

  editSpec(id:number){
    this.route.navigate([`/spec/edit/${id}`]);
  }

}
