import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Specialization } from '../specialization';
import { SpecializationService } from '../specialization.service';

@Component({
  selector: 'app-spec-edit',
  templateUrl: './spec-edit.component.html',
  styleUrls: ['./spec-edit.component.css']
})
export class SpecEditComponent implements OnInit {

  constructor(private service: SpecializationService,
            private route: Router,
            private activatedRouter: ActivatedRoute) { }

  spec:Specialization = new Specialization();
  id:number = 0;
  message:string = ""

  ngOnInit(): void {
    this.id = this.activatedRouter.snapshot.params['id'];
    this.service.getOneSpecialization(this.id).subscribe(
      data=>{
        this.spec= data;
      }
    )
  }

  updateSpec(){
    this.service.updateSpecialization(this.spec).subscribe(
      data=>{
        this.message = data;
        this.route.navigate(['/spec/all'])
      },error=>{
        this.message = error.statusText
      }
    );
  }

}
