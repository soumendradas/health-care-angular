import { Component, OnInit } from '@angular/core';
import { Specialization } from '../specialization';
import { SpecializationService } from '../specialization.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-spec-register',
  templateUrl: './spec-register.component.html',
  styleUrls: ['./spec-register.component.css']
})
export class SpecRegisterComponent implements OnInit {

  spec:Specialization = new Specialization();
  constructor(private specService: SpecializationService,
    private fb: FormBuilder) { }

  message:string = "";

  specForm: FormGroup = this.fb.group({
    code: ["", Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern("[A-Z]{1,10}")])],
    name: ["", Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern("[A-Za-z\s\.\&]{3,50}")])],
    note: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(500)])]
  });

  ngOnInit(): void {
  }

  saveSpec(specForm:any){
    this.spec.specCode = specForm.get('code').value;
    this.spec.specName = specForm.get('name').value;
    this.spec.specNote = specForm.get('note').value;
    this.specService.saveSpecialization(this.spec).subscribe(
      data=>{
        this.message = data;
        specForm.reset();
      },
      error=>{
        this.message = error.statusText;
        console.log(error);
      }
    );
  }

}
