import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecRegisterComponent } from './spec-register/spec-register.component';
import { SpecDataComponent } from './spec-data/spec-data.component';
import { SpecEditComponent } from './spec-edit/spec-edit.component';

import {ReactiveFormsModule, FormsModule} from '@angular/forms'
import { SpecializationRoutingModule } from './specialization-routing.module';


@NgModule({
  declarations: [
    SpecRegisterComponent,
    SpecDataComponent,
    SpecEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SpecializationRoutingModule
  ],
  exports: [
    SpecRegisterComponent
  ]
})
export class SpecializationModule { }
