import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Specialization } from 'src/app/specialization/specialization';
import { Doctor } from '../doctor';
import { DoctorService } from '../doctor.service';

@Component({
  selector: 'app-doctor-edit',
  templateUrl: './doctor-edit.component.html',
  styleUrls: ['./doctor-edit.component.css']
})
export class DoctorEditComponent implements OnInit {

  constructor(private service:DoctorService,
              private route: Router,
              private activatedRouter:ActivatedRoute) { }
  
  doc:Doctor = new Doctor();
  id:number = 0;
  allSpecs:Specialization[] = [];

  ngOnInit(): void {
    this.id = this.activatedRouter.snapshot.params['id'];
    this.service.getOneDoctor(this.id).subscribe(
      data=>{
        this.doc = data;
      }
    )
    this.service.getAllSpecNameAndData().subscribe(
      data=>{
        this.allSpecs = data;
        
      }
    )
  }

  updateDoctor(){

  }

}
