import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Doctor } from '../doctor';
import { DoctorService } from '../doctor.service';

@Component({
  selector: 'app-doctor-data',
  templateUrl: './doctor-data.component.html',
  styleUrls: ['./doctor-data.component.css']
})
export class DoctorDataComponent implements OnInit {

  docs:Doctor[]=[];
  message:string = ""
  constructor(private service:DoctorService,
            private route: Router) { }

  ngOnInit(): void {
    this.getAllDocs();

  }

  getAllDocs(){
    this.service.getAllDoctor().subscribe(
      data=>{
        this.docs = data;
      }
    )
  }

  trackById(index:number, doc:Doctor):number{
    return doc.id;
  }

  deleteDoc(id:number){
    this.service.deleteDoctor(id).subscribe(
      data=>{
        this.message = data;
        this.getAllDocs()
      },error=>{
        this.message = error;
      }
    )
  }

  editDoc(id:number){
    this.route.navigate([`doctor/edit/${id}`]);
  }

}
