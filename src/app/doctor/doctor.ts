import { Specialization } from "../specialization/specialization";

export class Doctor {

    id:number=0;
    name:string = "";
    gender:string = "";
    email:string = "";
    mobile:string = "";
    address:string = "";
    note:string = "";
    specialization:Specialization=new Specialization();

    constructor(){}
}
