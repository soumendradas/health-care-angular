import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { DoctorDataComponent } from './doctor-data/doctor-data.component';
import { DoctorEditComponent } from './doctor-edit/doctor-edit.component';
import { DoctorRegisterComponent } from './doctor-register/doctor-register.component';


const routes: Routes =[
  {path: 'doctor', 
  children: [
    {path:"register", component:DoctorRegisterComponent},
    {path:"all", component:DoctorDataComponent},
    {path:"edit/:id", component:DoctorEditComponent}
  ]
}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
