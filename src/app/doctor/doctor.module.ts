import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorRegisterComponent } from './doctor-register/doctor-register.component';
import { DoctorDataComponent } from './doctor-data/doctor-data.component';
import { DoctorEditComponent } from './doctor-edit/doctor-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DoctorRoutingModule } from './doctor-routing.module';



@NgModule({
  declarations: [
    DoctorRegisterComponent,
    DoctorDataComponent,
    DoctorEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DoctorRoutingModule
  ]
})
export class DoctorModule { }
