import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Doctor } from './doctor';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http:HttpClient) { }

  baseUrl:string = "http://localhost:9090/doctor/";

  getAllSpecNameAndData():Observable<any>{
    return this.http.get(`${this.baseUrl}getSpecs`);
  }

  saveDoctor(doctor:Doctor):Observable<any>{
    return this.http.post(`${this.baseUrl}save`, doctor, {responseType:'text'});
  }

  getAllDoctor():Observable<Doctor[]>{
    return this.http.get<Doctor[]>(`${this.baseUrl}all`);
  }

  deleteDoctor(id:number):Observable<any>{
    return this.http.delete(`${this.baseUrl}delete/${id}`, {responseType:'text'});
  }

  getOneDoctor(id:number):Observable<Doctor>{
    return this.http.get<Doctor>(`${this.baseUrl}getDoctor/${id}`);
  }

}
