import { Component, OnInit } from '@angular/core';
import { Specialization } from 'src/app/specialization/specialization';
import { Doctor } from '../doctor';
import { DoctorService } from '../doctor.service';

@Component({
  selector: 'app-doctor-register',
  templateUrl: './doctor-register.component.html',
  styleUrls: ['./doctor-register.component.css']
})
export class DoctorRegisterComponent implements OnInit {

  constructor(private service:DoctorService) { }
  allSpecs:Specialization[] = [];

  doc:Doctor=new Doctor();
  message:string = "";

  ngOnInit(): void {
    this.service.getAllSpecNameAndData().subscribe(
      data=>{
        this.allSpecs = data;
        console.log(this.allSpecs)
      }
    )
  }

  saveDoctor(){
    this.service.saveDoctor(this.doc).subscribe(
      data=>{
        this.message = data;
      }, error=>{
        this.message = error.statusText;
      }
    )
  }

  

}
