import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientRegisterComponent } from './patient-register/patient-register.component';
import { PatientDataComponent } from './patient-data/patient-data.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PatientRoutingModule } from './patient-routing.module';


@NgModule({
  declarations: [
    PatientRegisterComponent,
    PatientDataComponent,
    PatientEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    PatientRoutingModule
  ]
})
export class PatientModule { }
