import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientDataComponent } from './patient-data/patient-data.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { PatientRegisterComponent } from './patient-register/patient-register.component';

const routes:Routes = [
  {path:'patient',
  children:[
    {path:"register", component:PatientRegisterComponent},
    {path:"all", component:PatientDataComponent},
    {path:"edit/:id", component:PatientEditComponent}
  ]
}
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
