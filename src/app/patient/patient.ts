export class Patient {

    patId:number = 0;
    firstName:string = "";
    lastName:string ="";
    gender:string = "";
    mobile:string = "";
    email:string = "";
    martialStatus:string = "";
    presAddress:string = "";
    commAddress:string = "";
    ifOther:string = "";
    note:string = "";
    medicalHist:string[] = [];
    dob:string = "";

    constructor(){}

}
