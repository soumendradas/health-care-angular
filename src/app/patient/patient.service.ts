import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Patient } from './patient';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http:HttpClient) { }

  baseUrl:string = "http://localhost:9090/patient/"

  savePatient(pat:Patient):Observable<any>{
    return this.http.post(`${this.baseUrl}save`, pat, {responseType:'text'});
  }

  // getPatient(id:number):Observable<Patient>{
  //   return this.http.get<Patient>(`${this.baseUrl}getPatient/${id}`);
  // }

  getAllPatient():Observable<Patient[]>{
    return this.http.get<Patient[]>(`${this.baseUrl}all`);
  }
}
