import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { PatientService } from '../patient.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'

@Component({
  selector: 'app-patient-register',
  templateUrl: './patient-register.component.html',
  styleUrls: ['./patient-register.component.css']
})
export class PatientRegisterComponent implements OnInit {

  constructor(private service:PatientService,
            private fb: FormBuilder) { }

  
  patient:Patient = new Patient();
  regForm:FormGroup = this.fb.group({
    fName:["", Validators.compose([Validators.required, Validators.maxLength(30)])],
    lName:["",Validators.compose([Validators.required, Validators.maxLength(20)])],
    gender:["", Validators.required],
    mobile:["", Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern("[0-9]{10}")])],
    email:["", Validators.compose([Validators.required, Validators.email, Validators.maxLength(50)])],
    dob:["",Validators.required],
    martialStatus:["", Validators.required],
    presAddress:["", Validators.required],
    commAddress:["", Validators.required],
    note:["", Validators.required]
  })
  message:string = "";
  
  ngOnInit(): void {
   
  }

  savePatient(regForm:any){
    this.patient.firstName = regForm.get("fName").value;
    this.patient.lastName=regForm.get("lName").value;
    this.patient.gender = regForm.get("gender").value;
    this.patient.mobile = regForm.get("mobile").value;
    this.patient.email = regForm.get("email").value;
    this.patient.dob = regForm.get("dob").value;
    this.patient.martialStatus = regForm.get("martialStatus").value;
    this.patient.presAddress = regForm.get("presAddress").value;
    this.patient.commAddress = regForm.get("commAddress").value;
    this.patient.note = regForm.get("note").value;
    console.log(this.patient);
    this.service.savePatient(this.patient).subscribe(
      data=>{
        this.message = data;
        this.regForm.reset();
      },error=>{
        this.message = error;
      }
    )
  }

}
