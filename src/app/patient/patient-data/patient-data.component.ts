import { Component, OnInit } from '@angular/core';
import { Patient } from '../patient';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-patient-data',
  templateUrl: './patient-data.component.html',
  styleUrls: ['./patient-data.component.css']
})
export class PatientDataComponent implements OnInit {

  constructor(private service: PatientService) { }

  patients: Patient[] = [];
  message:string = "";

  ngOnInit(): void {
    this.getAllPatient();
  }

  getAllPatient():void {
    this.service.getAllPatient().subscribe(
      data => {
        this.patients = data;
      },error=>{
        this.message = error;
      }
    )
  }

}
